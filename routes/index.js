var express = require('express');
var path = require('path');
var router = express.Router();
var satelize = require('satelize');
var fs = require('fs');

var uip = '';
var latitude = '';
var longitude = '';
var lastLogin = '';
var firstname = '';
var surname = '';
var email = '';

var latitude1 = '';
var longitude1 = '';
var lastLogin1 = '';
var firstname1 = '';
var surname1 = '';
var email1 = '';

var latitude2 = '';
var longitude2 = '';
var lastLogin2 = '';
var firstname2 = '';
var surname2 = '';
var email2 = '';

var latitude3 = '';
var longitude3 = '';
var lastLogin3 = '';
var firstname3 = '';
var surname3 = '';
var email3 = '';

var latitude4 = '';
var longitude4 = '';
var lastLogin4 = '';
var firstname4 = '';
var surname4 = '';
var email4 = '';

var u1;
var u2;
var u3;
var u4;
var u5;


/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.oidc.isAuthenticated());
  if(req.oidc.isAuthenticated()){
    uip = req.headers['x-forwarded-for'] ||
       req.socket.remoteAddress ||
       null;
    

    var data = fs.readFileSync('./user4.json'),
       myObj;
   
    try {
     myObj = JSON.parse(data);
     console.dir(myObj);
    }
    catch (err) {
       console.log('There has been an error parsing your JSON.')
       console.log(err);
    }

    latitude4 = myObj['latitude'];
    longitude4 = myObj['longitude'];
    lastLogin4 = myObj['lastLogin'];
    firstname4 = myObj['firstname'];
    surname4 = myObj['surname'];
    email4 = myObj['email'];

    var data = fs.readFileSync('./user3.json'),
       myObj;
   
    try {
     myObj = JSON.parse(data);
     console.dir(myObj);
    }
    catch (err) {
       console.log('There has been an error parsing your JSON.')
       console.log(err);
    }

    latitude3 = myObj['latitude'];
    longitude3 = myObj['longitude'];
    lastLogin3 = myObj['lastLogin'];
    firstname3 = myObj['firstname'];
    surname3 = myObj['surname'];
    email3 = myObj['email'];

    var data = fs.readFileSync('./user2.json'),
       myObj;
   
    try {
     myObj = JSON.parse(data);
     console.dir(myObj);
    }
    catch (err) {
       console.log('There has been an error parsing your JSON.')
       console.log(err);
    }

    latitude2 = myObj['latitude'];
    longitude2 = myObj['longitude'];
    lastLogin2 = myObj['lastLogin'];
    firstname2 = myObj['firstname'];
    surname2 = myObj['surname'];
    email2 = myObj['email'];

    var data = fs.readFileSync('./user1.json'),
       myObj;
   
    try {
     myObj = JSON.parse(data);
     console.dir(myObj);
    }
    catch (err) {
       console.log('There has been an error parsing your JSON.')
       console.log(err);
    }

    latitude1 = myObj['latitude'];
    longitude1 = myObj['longitude'];
    lastLogin1 = myObj['lastLogin'];
    firstname1 = myObj['firstname'];
    surname1 = myObj['surname'];
    email1 = myObj['email'];

    satelize.satelize({ip: uip}, function(err, payload){
      var string = JSON.stringify(payload);
      var objectValue = JSON.parse(string);
      latitude = objectValue['latitude'];
      longitude = objectValue['longitude'];
      string = JSON.stringify(req.oidc.user);
      objectValue = JSON.parse(string);
      lastLogin = objectValue['updated_at'];
      firstname = objectValue['given_name'];
      surname = objectValue['family_name'];
      email = objectValue['email'];
      });

    var myOptions = {
      latitude: latitude4,
      longitude: longitude4,
      lastLogin: lastLogin4,
      firstname: firstname4,
      surname: surname4,
      email: email4
    };
      
    var data = JSON.stringify(myOptions);
    u5 = myOptions;
      
    fs.writeFile('./user5.json', data, function (err) {
      if (err) {
        console.log('There has been an error saving your configuration data.');
        console.log(err.message);
        return;
      }
      console.log('Configuration saved successfully.')
    });

    var myOptions = {
      latitude: latitude3,
      longitude: longitude3,
      lastLogin: lastLogin3,
      firstname: firstname3,
      surname: surname3,
      email: email3
    };
      
    var data = JSON.stringify(myOptions);
    u4 = myOptions;
      
    fs.writeFile('./user4.json', data, function (err) {
      if (err) {
        console.log('There has been an error saving your configuration data.');
        console.log(err.message);
        return;
      }
      console.log('Configuration saved successfully.')
    });

    var myOptions = {
      latitude: latitude2,
      longitude: longitude2,
      lastLogin: lastLogin2,
      firstname: firstname2,
      surname: surname2,
      email: email2
    };
      
    var data = JSON.stringify(myOptions);
    u3 = myOptions;
      
    fs.writeFile('./user3.json', data, function (err) {
      if (err) {
        console.log('There has been an error saving your configuration data.');
        console.log(err.message);
        return;
      }
      console.log('Configuration saved successfully.')
    });

    var myOptions = {
      latitude: latitude1,
      longitude: longitude1,
      lastLogin: lastLogin1,
      firstname: firstname1,
      surname: surname1,
      email: email1
    };
      
    var data = JSON.stringify(myOptions);
    u2 = myOptions;
      
    fs.writeFile('./user2.json', data, function (err) {
      if (err) {
        console.log('There has been an error saving your configuration data.');
        console.log(err.message);
        return;
      }
      console.log('Configuration saved successfully.')
    });

    var myOptions = {
      latitude: latitude,
      longitude: longitude,
      lastLogin: lastLogin,
      firstname: firstname,
      surname: surname,
      email: email
    };
      
    var data = JSON.stringify(myOptions);
    u1 = myOptions;
      
    fs.writeFile('./user1.json', data, function (err) {
      if (err) {
        console.log('There has been an error saving your configuration data.');
        console.log(err.message);
        return;
      }
      console.log('Configuration saved successfully.')
    });
  }
  res.render('index.ejs', {
    latitude: latitude,
    longitude: longitude,
    lastLogin: lastLogin,
    firstname: firstname,
    surname: surname,
    email: email,
    latitude2: latitude1,
    longitude2: longitude1,
    lastLogin2: lastLogin1,
    firstname2: firstname1,
    surname2: surname1,
    email2: email1,
    latitude3: latitude2,
    longitude3: longitude2,
    lastLogin3: lastLogin2,
    firstname3: firstname2,
    surname3: surname2,
    email3: email2,
    latitude4: latitude3,
    longitude4: longitude3,
    lastLogin4: lastLogin3,
    firstname4: firstname3,
    surname4: surname3,
    email4: email3,
    latitude5: latitude4,
    longitude5: longitude4,
    lastLogin5: lastLogin4,
    firstname5: firstname4,
    surname5: surname4,
    email5: email4,
    isAuthenticated: req.oidc.isAuthenticated(),
    user: req.oidc.user,
    u1: u1,
    u2: u2,
    u3: u3,
    u4: u4,
    u5: u5,
  });
});

module.exports = router;
