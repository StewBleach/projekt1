var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var satelize = require('satelize');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
require('dotenv').config();
const { auth, requiresAuth } = require('express-openid-connect');

app.use(
  auth({
    issuerBaseURL: process.env.ISSUER_BASE_URL,
    baseURL: process.env.BASE_URL,
    clientID: process.env.CLIENT_ID,
    secret: process.env.SECRET,
    authRequired: false,
    idpLogout: true,
  })
);

/*
app.get('/', (req, res) => {
  res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out');
});
*/

app.get('/profile', requiresAuth(), (req, res) =>{
  res.send(JSON.stringify(req.oidc.user));
});

/*
var uip = '';
var latitude = '';
var longitude = '';
var lastLogin = '';
var firstname = '';
var surname = '';
var email = '';

app.get('/', (req,res,next) => {
  if(req.oidc.isAuthenticated()){
  uip = req.headers['x-forwarded-for'] ||
     req.socket.remoteAddress ||
     null;

  satelize.satelize({ip: uip}, function(err, payload){
    var string = JSON.stringify(payload);
    var objectValue = JSON.parse(string);
    latitude = objectValue['latitude'];
    longitude = objectValue['longitude'];
    string = JSON.stringify(req.oidc.user);
    objectValue = JSON.parse(string);
    lastLogin = objectValue['updated_at'];
    firstname = objectValue['given_name'];
    surname = objectValue['family_name'];
    email = objectValue['email'];
  });
  }else{}
  next();
});

var info = latitude + ";;" + longitude + ";;" + lastLogin + ";;" + firstname + ";;" + surname + ";;" + email;*/

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`listening on port ${port}`);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
